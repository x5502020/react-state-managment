import axios from "axios";
import React, { useEffect, useState } from "react";
import "./Community.css";

const Community = () => {
  const [data, setData] = useState([]);
  const [isSectionVisible, setIsSectionVisible] = useState(true);
  useEffect(() => {
    axios
      .get("http://localhost:8080/community")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching community data:", error);
      });
  }, []);

  const toggleSectionVisibility = () => {
    setIsSectionVisible(!isSectionVisible);
  };

  return (
    <div className="community">
      <h1>Big Community of People Like You</h1>
      {isSectionVisible && (
        <p>
          We’re proud of our products, and we’re really excited when we get
          feedback from our users.
        </p>
      )}
      <div className="button-container">
        <button onClick={toggleSectionVisibility}>
          {isSectionVisible ? "Hide section" : "Show section"}
        </button>
      </div>
      {isSectionVisible && (
        <div className="community-cards">
          {data.map((item) => (
            <div key={item.id} className="card">
              <img src={item.avatar} alt={`Avatar of ${item.firstName}`} />
              <p>{`${item.firstName} ${item.lastName}`}</p>
              <p>{item.position}</p>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default Community;
