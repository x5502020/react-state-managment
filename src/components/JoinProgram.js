import React, { useState } from "react";
import axios from "axios";

//Styles
import "./JoinProgram.css";

const JoinProgram = () => {
  const [email, setEmail] = useState("");
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setEmail(e.target.value);
  };

  const handleSubmit = async (e, action) => {
    e.preventDefault();
    const data = { email };

    try {
      const endpoint = action === "subscribe" ? "/subscribe" : "/unsubscribe";
      setIsLoading(true);

      await axios.post(`http://localhost:8080${endpoint}`, data);

      if (action === "subscribe") {
        setIsSubscribed(true);
      } else {
        setIsSubscribed(false);
      }

      setEmail("");
    } catch (error) {
      console.error(
        `${action === "subscribe" ? "Subscription" : "Unsubscription"} failed`,
        error
      );
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="join-program">
      <div className="join-program-background">{}</div>
      <div className="join-program-content">
        <h2>Join our program</h2>
        <p className="p">
          Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>

        <form
          onSubmit={(e) =>
            handleSubmit(e, isSubscribed ? "unsubscribe" : "subscribe")
          }
        >
          <input
            type="email"
            placeholder="Email"
            value={email}
            onChange={handleChange}
          />
          <button type="submit" disabled={isLoading}>
            {isLoading
              ? "Processing..."
              : isSubscribed
              ? "Unsubscribe"
              : "Subscribe"}
          </button>
        </form>
      </div>
    </div>
  );
};

export default JoinProgram;
