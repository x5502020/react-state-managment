import Community from "./components/Community";
import JoinProgram from "./components/JoinProgram";
function App() {
  return (
    <div>
      <Community />
      <JoinProgram />
    </div>
  );
}

export default App;
